# Jehanne

Jehanne is a [simple][simplicity] operating system.

Jehanne has noble ancestors:

- most of userland tools, a lot of wisdom and some kernel modules,
  come from [9front][9front]
- the kernel is a fork of Charles Forsyth's [Plan9-9k][plan9-9k]
- ~~most of the build system and~~ some valuable piece of code come from [Harvey OS][harvey]

Still the project is named after a humble peasant,
the famous French heretic [Joan of Arc][arc], because it diverges deeply
from the design and conventions of its predecessors.

## Overview

This is the main repository, used to build the system as a whole:

- [arch](./arch/) contains one folder for each
  supported architecture, with specific C headers, libraries and executables
  (note that by architecture we intend any kind of physical or virtual
  machine that is able to run code, thus rc is actually an architecture)
- [sys](./sys) is the system folder
    * [include](./sys/include) contains portable C headers
    * [lib](./sys/lib) contains data and scripts used by the
      running system
    * [man](./sys/man) contains manual pages
    * [src](./sys/src) contains the sources of the system
- [doc](./doc/) contains useful documentation for Jehanne
  development
    * [license](./doc/license/) contains detailed info
      about Jehanne [licenses][lic]
    * [hacking](./doc/hacking/) contains details about how
      to build and modify Jehanne
- [hacking](./hacking) contains the utilities used to
  develop Jehanne
- [qa](./qa) contains the regression tests
- [mnt](./mnt) contains default mount targets
- [usr](./usr) contains the users' folders
- [pkgs](./pkgs) will contains the installed packages

The running system also includes supplemental folders like `/lib`,
`/cmd` and `/dev` that are bound during the boot as required.

## Build

To build Jehanne and play with it, you need to have git, qemu,
gcc, binutils and bison installed.
For example on Debian GNU/Linux 10 you should be able to get going with

	sudo apt-get install libxt-dev libtool-bin git build-essential flex bison qemu-system autoconf autoconf-archive curl automake

After the repository clone, you can give a look with

	git submodule init                               # we have a lot of submodules
	git submodule update --init --recursive --remote --depth 1
	./hacking/devshell.sh                            # start a shell with appropriate environment
	./hacking/continuous-build.sh                    # to build everything (will take a while)
	./hacking/runOver9P.sh                           # to start the system in QEMU
	./hacking/drawterm.sh                            # to connect Jehanne with drawterm

## Hacking

Jehanne is a work in progress.  

The limited amount of time that I can dedicate to its development and the 
[pending licensing issues](http://jehanne.h--k.it/2022/01/06/jehanne_is_moving.html#leaving-github) made me
realize that Jehanne is really a **personal** Research OS, as it was correctly defined,
years ago, by [Aiju](https://aiju.de/about_me/).

So I decided to adopt the [contribution strategy](https://github.com/klange/toaruos#are-contributions-accepted)
of a more mature OS, like [TuaruOS](https://toaruos.org/) is: contributions in the form of code should be discussed in advance.
Feel free to [reach me on the Fediverse](https://qoto.org/@Shamar)!

However, if you really want to contribute to Jehanne, my suggestion is to contribute to [9front](https://9front.org) first:
9front is the most mature and stable among Plan 9's evolutions, it's developed by great hackers with a [great sense of humor](https://fqa.9front.org/fqa0.html#0.1). :-D

Contributing to 9front will be much more fun and formative than contributing to Jehanne, and I will happily port your code when ready.




[simplicity]: http://plato.stanford.edu/entries/simplicity/ "What is simplicity?"
[harvey]: http://harvey-os.org "Harvey OS"
[9front]: http://9front.org/ "THE PLAN FELL OFF"
[plan9-9k]: https://bitbucket.org/forsyth/plan9-9k "Experimental 64-bit Plan 9 kernel"
[nix]: https://github.com/rminnich/nix-os
[arc]: https://en.wikipedia.org/wiki/Joan_of_Arc "Jeanne d'Arc"
[lic]: ./LICENSE.md "A summary of Jehanne licensing"


